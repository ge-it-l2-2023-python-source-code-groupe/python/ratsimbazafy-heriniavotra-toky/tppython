hauteur = 10

for i in range(1, hauteur + 1):
    espaces = hauteur - i
    etoiles = 2 * i - 1
    ligne = ' ' * espaces + '*' * etoiles
    print(ligne)
